# Bookshop Acceptance Tests

Bookshop acceptance tests provide acceptance tests for bookshop api which is a simple crud operations service

## Prerequisites

- Java 8+
- Maven
- intellij plugins
  - java-cucumber - cucumber support for java
  - gherkin - same as above

## Overrides

Project is configurable, these are the options:

- environment = which environment to run the tests against, default value: dev
- tags = which features/test suites to run, default value: `@smoke`

# Observations/Notes/Assumptions for assessment

## Notes
For negative tests there should really be shift left approach where unit/integration coverage is high enough to cover
basics like bad pathing or schema, this can be achieved through property based testing

More testing could have been done e.g. different charsets or experimental method types like lock,
but it comes back to my first point as well as exploratory testing

## Observations
- flaky api response e.g. add book api sometimes returns higher decimal point than provided,
there's a number of ways to address this, in interest of time I did not implement this, lets discuss in next round :)
- delete api either returns plaintext or json object
- empty json should be bad request not server error
- id should never be integer as you will run out at some point

## Assumptions
- not sure what's the bounds of ids, presuming there are some acceptance criteria e.g. no negatives or too high numbers