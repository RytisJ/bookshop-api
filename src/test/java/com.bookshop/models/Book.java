package com.bookshop.models;

import lombok.*;
import lombok.extern.jackson.Jacksonized;

/**
 * Bookshop api response model
 */
@Jacksonized
@Data
@Builder(toBuilder = true)
public class Book {
    private Integer id;
    private final String name;
    private final String author;
    private final String publication;
    private final String category;
    private final Integer pages;
    private final Double price;
}
