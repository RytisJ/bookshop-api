package com.bookshop.models;

import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

/**
 * Bookshop api error response model
 */
@Jacksonized
@Data
@Builder
public class BookshopErrorModel {
    private final String timestamp;
    private final Integer status;
    private final String error;
    private final String path;
}
