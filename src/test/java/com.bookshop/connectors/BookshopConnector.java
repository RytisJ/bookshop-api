package com.bookshop.connectors;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import com.bookshop.models.Book;
import net.serenitybdd.rest.SerenityRest;
import com.bookshop.utils.Config;

import java.util.Map;

/**
 * All the api calls for bookshop api
 */
public class BookshopConnector {

    private final RequestSpecification apiBuilder;

    /**
     * Simple way of building connector for happy path testing
     */
    public BookshopConnector() {
        Config config = new Config();
        this.apiBuilder = SerenityRest.given()
                .baseUri(config.getConfigValue("bookshopApi.baseUri"))
                .auth().basic(config.getConfigValue("bookshopApi.username"), config.getConfigValue("bookshopApi.password"))
                .port(Integer.parseInt(config.getConfigValue("bookshopApi.port")))
                .contentType(ContentType.JSON);
    }

    /**
     * More vanilla way of using the connector, mainly for negative testing
     * @param baseUri base uri for bookshop api
     * @param port port number
     * @param headers headers
     * @param username username for basic auth
     * @param password password for basic auth
     */
    public BookshopConnector(String baseUri, Integer port, Map<String, ?> headers, String username, String password) {
        this.apiBuilder = SerenityRest.given()
                .baseUri(baseUri)
                .auth().basic(username, password)
                .port(port)
                .headers(headers);
    }


    /**
     * Adds a book to bookshop
     * @param book contains all information around the book being added
     * @return what you put in with addition of ID
     */
    public Response addBook(String book) {
        return apiBuilder.body(book).post("/api/v1/books").thenReturn();
    }

    /**
     * view/open a book to see its information via ID
     * @param id of the book
     * @return information about a book which can be parsed to Book pojo
     */
    public Response viewBook(Object id) {
        return apiBuilder.get("/api/v1/books/" + id).thenReturn();
    }

    /**
     * view all books within bookshop
     * @return list of book objects
     */
    public Response viewAllBooks() {
        return apiBuilder.get("/api/v1/books").thenReturn();
    }

    /**
     * Updates content of the book via id
     * @param id of the book being updated
     * @param body payload
     * @return same information as you put in
     */
    public Response modifyBook(Object id, Object body) {
        return apiBuilder.body(body).put("/api/v1/books/" + id).thenReturn();
    }

    /**
     * Deletes a book from bookshop
     * @param id of the book you want to delete
     * @return confirmation message that you have successfully deleted a book in plaintext
     */
    public Response deleteBook(Object id) {
        return apiBuilder.delete("/api/v1/books/" + id).thenReturn();
    }
}
