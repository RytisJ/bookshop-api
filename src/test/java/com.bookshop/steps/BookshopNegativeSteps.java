package com.bookshop.steps;


import com.bookshop.models.Book;
import com.bookshop.utils.SharedStateConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.bookshop.connectors.BookshopConnector;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import com.bookshop.models.BookshopErrorModel;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import com.bookshop.utils.Config;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.bookshop.utils.SharedStateConstants.*;
import static org.assertj.core.api.Assertions.assertThat;


public class BookshopNegativeSteps {
    private final Faker faker = new Faker();
    private final Config config = new Config();
    private final ObjectMapper objectMapper = new ObjectMapper();

    private final BookshopConnector bookshopConnector = new BookshopConnector();

    @Steps
    private BookshopSteps bookshopSteps;

    @When("I add new book with using headers")
    public void iAddNewBookWithIncorrectHeaders() throws JsonProcessingException {
        Serenity
                .setSessionVariable(CREATE_BOOK_API_RESPONSE)
                .to(new BookshopConnector(
                        config.getConfigValue("bookshopApi.baseUri"),
                        Integer.parseInt(config.getConfigValue("bookshopApi.port")),
                        Collections.emptyMap(),
                        config.getConfigValue("bookshopApi.username"),
                        config.getConfigValue("bookshopApi.password")
                ).addBook(objectMapper.writeValueAsString(bookshopSteps.createRandomBook()))
        );
    }

    @Then("I can see {string} when {}")
    public void iShouldSeeError(String errorType, String apiType) throws Exception {

        SharedStateConstants apiResponse;
        String path;
        switch (apiType) {
            case "creating a new book":
                apiResponse = CREATE_BOOK_API_RESPONSE;
                path = "/api/v1/books";
                break;
            case "viewing a book":
                apiResponse = VIEW_BOOK_API_RESPONSE;
                path = "/api/v1/books/" + Serenity.sessionVariableCalled(BOOK_ID);
                break;
            case "deleting a book":
                apiResponse = DELETE_BOOK_API_RESPONSE;
                path = "/api/v1/books/" + Serenity.sessionVariableCalled(BOOK_ID);
                break;
            case "updating a book":
                apiResponse = UPDATE_BOOK_API_RESPONSE;
                path = "/api/v1/books/" + Serenity.sessionVariableCalled(BOOK_ID);
                break;
            default:
                throw new Exception("API response: " + apiType + " is not defined");
        }


        BookshopErrorModel expectedBookshopErrorModel;
        switch (errorType) {
            case "Bad Request":
                expectedBookshopErrorModel = BookshopErrorModel.builder()
                        .status(400)
                        .error("Bad Request")
                        .path(path)
                        .build();
                break;
            case "Not Found":
                expectedBookshopErrorModel = BookshopErrorModel.builder()
                        .status(404)
                        .error("Not Found")
                        .path(path)
                        .build();
                break;
            default:
                throw new Exception("Error type: " + errorType + " is not supported");
        }


        Response response = Serenity.sessionVariableCalled(apiResponse);
        assertThat(response.statusCode())
                .as("checking status code").isEqualTo(expectedBookshopErrorModel.getStatus());
        BookshopErrorModel parsedResponse = response.as(BookshopErrorModel.class);
        assertThat(parsedResponse).isEqualToIgnoringNullFields(expectedBookshopErrorModel);

        // testing timestamps is a bit more difficult, so parsing alone is good enough for seconds and nanos
        ZonedDateTime currentTime = ZonedDateTime.now().minus(1, ChronoUnit.HOURS);
        ZonedDateTime apiTimestamp = ZonedDateTime.parse(parsedResponse.getTimestamp());

        // comparing what we can compare is important in case it returns 1999 year etc...
        assertThat(currentTime.getYear()).as("testing error timestamp year")
                .isEqualTo(apiTimestamp.getYear());
        assertThat(currentTime.getMonth()).as("testing error timestamp month")
                .isEqualTo(apiTimestamp.getMonth());
        assertThat(currentTime.getDayOfMonth()).as("testing error timestamp day")
                .isEqualTo(apiTimestamp.getDayOfMonth());
        assertThat(currentTime.getHour()).as("testing error timestamp hour")
                .isEqualTo(apiTimestamp.getHour());
        assertThat(currentTime.getMinute()).as("testing error timestamp minute")
                .isEqualTo(apiTimestamp.getMinute());

    }

    @When("I add new book using empty body")
    public void iAddNewBookWithEmptyBody() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        Serenity.setSessionVariable(CREATE_BOOK_API_RESPONSE).to(
                new BookshopConnector(
                        config.getConfigValue("bookshopApi.baseUri"),
                        Integer.parseInt(config.getConfigValue("bookshopApi.port")),
                        headers,
                        config.getConfigValue("bookshopApi.username"),
                        config.getConfigValue("bookshopApi.password")).addBook("{}")
        );
    }

    @When("I add a book with author set as boolean")
    public void iAddABookWithAuthorSetAsBoolean() throws JsonProcessingException {
        Book filledBook = Serenity.sessionVariableCalled(INITIAL_BOOK);
        String bookAsJson = objectMapper.writeValueAsString(filledBook);
        TypeReference<HashMap<String, Object>> typeRef
                = new TypeReference<HashMap<String, Object>>() {
        };
        Map<String, Object> bookAsMap = objectMapper.readValue(bookAsJson, typeRef);
        bookAsMap.replace("author", true);

        bookshopConnector.addBook(objectMapper.writeValueAsString(bookAsMap));
    }

    @When("I add a book with really long name")
    public void iAddABookWithReallyLongName() throws JsonProcessingException {
        Book book = Serenity.sessionVariableCalled(INITIAL_BOOK);
        Serenity
                .setSessionVariable(CREATE_BOOK_API_RESPONSE)
                .to(bookshopConnector
                        .addBook(objectMapper.writeValueAsString(
                                book.toBuilder().name(faker.regexify("[a-zA-Z0-10]{300}")).build())));
    }

    @Given("I am {} a book that {}")
    public void iAmXABookThat(String typeOfAction, String typeOfId) throws Exception {
        Object id;
        switch (typeOfId) {
            case "does not exist":
                List<Integer> listOfIds = Stream.of(bookshopConnector.viewAllBooks().as(Book[].class))
                        .map(Book::getId)
                        .sorted()
                        .collect(Collectors.toList());
                // making an assumption that there's not that many books being created at the same time
                id = listOfIds.get(listOfIds.size() -1) + 1000;
                break;
            case "has non standard id":
                id = faker.regexify("[a-zA-Z0-10]{10}");
                break;
            case "null":
                id = "null";
                break;
            case "empty":
                id = "  ";
                break;
            case "out of bounds - high bound":
                id = Integer.MAX_VALUE;
                break;
            case "out of bounds - low bound":
                id = Integer.MIN_VALUE;
                break;
            default:
                throw new Exception("Type of test: " + typeOfId + " is not defined");
        }

        Serenity.setSessionVariable(BOOK_ID).to(id);


        switch (typeOfAction) {
            case "viewing a book":
                Serenity.setSessionVariable(VIEW_BOOK_API_RESPONSE).to(bookshopConnector.viewBook(id));
                break;
            case "deleting a book":
                Serenity.setSessionVariable(DELETE_BOOK_API_RESPONSE).to(bookshopConnector.deleteBook(id));
                break;
            case "updating a book":
                Book book = bookshopSteps.createRandomBook();
//                String bookAsJson = objectMapper.writeValueAsString(book);
//                TypeReference<HashMap<String, Object>> typeRef
//                        = new TypeReference<HashMap<String, Object>>() {
//                };
//                Map<String, Object> bookAsMap = objectMapper.readValue(bookAsJson, typeRef);
//                bookAsMap.replace("id", id);

                Serenity.setSessionVariable(UPDATE_BOOK_API_RESPONSE)
                        .to(bookshopConnector.modifyBook(id, book));
                break;
            default:
                throw new Exception("Action: " + typeOfAction + " not mapped");

        }


    }
}
