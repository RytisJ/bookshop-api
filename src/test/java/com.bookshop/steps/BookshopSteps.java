package com.bookshop.steps;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.bookshop.connectors.BookshopConnector;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import io.restassured.response.Response;
import com.bookshop.models.Book;
import lombok.SneakyThrows;
import net.serenitybdd.core.Serenity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static com.bookshop.utils.SharedStateConstants.*;


public class BookshopSteps {
    private final Faker faker = new Faker();
    private final BookshopConnector bookshopConnector = new BookshopConnector();

    private final ObjectMapper objectMapper = new ObjectMapper();

    public Book createRandomBook() {
        return Book.builder()
                .name(faker.book().title())
                .publication(faker.book().publisher())
                .author(faker.book().author())
                .category(faker.company().industry()).pages(faker.number().numberBetween(100, 999))
                .price(faker.number().randomDouble(2, 1, 999))
                .build();
    }

    @Given("I want to add new book")
    public void iWantToAddNewBook() {
        Book testBook = createRandomBook();

        Serenity.setSessionVariable(INITIAL_BOOK).to(testBook);
    }

    @When("I add new book")
    public void iAddNewBook() throws JsonProcessingException {
        Book testBook = Serenity.sessionVariableCalled(INITIAL_BOOK);
        Serenity
                .setSessionVariable(CREATE_BOOK_API_RESPONSE)
                .to(bookshopConnector.addBook(objectMapper.writeValueAsString(testBook)));
    }

    @Then("my book is added")
    public void myBookIsAdded() {
        Book expectedBook = Serenity.sessionVariableCalled(INITIAL_BOOK);
        Response response = Serenity.sessionVariableCalled(CREATE_BOOK_API_RESPONSE);
        assertThat(response.statusCode()).isEqualTo(200);
        Book parsedResponse = response.as(Book.class);
        assertThat(parsedResponse).isEqualToIgnoringNullFields(expectedBook);
    }

    @Given("I have a book")
    public void iHaveABook() throws JsonProcessingException {
        iWantToAddNewBook();
        iAddNewBook();
        myBookIsAdded();
    }

    @When("I view the book")
    public void iViewTheBook() {

        Response response = Serenity.sessionVariableCalled(CREATE_BOOK_API_RESPONSE);
        Book parsedResponse = response.as(Book.class);

        Serenity.setSessionVariable(INITIAL_BOOK).to(parsedResponse);
        Serenity.setSessionVariable(VIEW_BOOK_API_RESPONSE).to(bookshopConnector.viewBook(parsedResponse.getId()));
    }

    @Then("I can see book information")
    public void iCanSeeBookInformation() {
        Response response = Serenity.sessionVariableCalled(VIEW_BOOK_API_RESPONSE);
        assertThat(response.statusCode()).isEqualTo(200);
        assertThat((Book) Serenity.sessionVariableCalled(INITIAL_BOOK)).isEqualTo(response.as(Book.class));
    }

    @SneakyThrows
    @When("I update books attributes")
    public void iUpdateBooksAttributes() {

        Response response = Serenity.sessionVariableCalled(CREATE_BOOK_API_RESPONSE);
        Book parsedResponse = response.as(Book.class);

        // take same data and add random values at the end
        // for price round up previous value and then generate value above
        // for pages same thing as price
        Book initialBookModified = Book
                .builder()
                .id(parsedResponse.getId())
                .name(parsedResponse.getName() + faker.random().nextInt(1000))
                .author(parsedResponse.getAuthor() + faker.random().nextInt(1000))
                .category(parsedResponse.getCategory() + faker.random().nextInt(1000))
                .publication(parsedResponse.getPublication() + faker.random().nextInt(1000))
                .price(faker
                        .number()
                        .randomDouble(
                                2,
                                Math.round(parsedResponse.getPrice()) + 1,
                                Math.round(parsedResponse.getPrice()) + 1001))
                .pages(faker.random().nextInt(parsedResponse.getPages() + 1, parsedResponse.getPages() + 1001))
                .build();

        Serenity.setSessionVariable(INITIAL_BOOK).to(initialBookModified);
        Serenity.setSessionVariable(UPDATE_BOOK_API_RESPONSE)
                .to(bookshopConnector.modifyBook(initialBookModified.getId(),
                        objectMapper.writeValueAsString(initialBookModified)));
    }

    @Then("I can see the updates to the book")
    public void iCanSeeTheUpdatesToTheBook() {
        Response response = Serenity.sessionVariableCalled(UPDATE_BOOK_API_RESPONSE);
        assertThat(response.statusCode()).isEqualTo(200);

        assertThat(response.as(Book.class)).isEqualTo(Serenity.sessionVariableCalled(INITIAL_BOOK));
    }

    @When("I delete the book")
    public void iDeleteTheBook() {
        Response response = Serenity.sessionVariableCalled(CREATE_BOOK_API_RESPONSE);
        Book parsedResponse = response.as(Book.class);

        Serenity.setSessionVariable(INITIAL_BOOK).to(parsedResponse);
        Serenity.setSessionVariable(DELETE_BOOK_API_RESPONSE).to(bookshopConnector.deleteBook(parsedResponse.getId()));
    }

    @Then("the book is deleted")
    public void theBookIsDeleted() {
        Response deleteBookApiResponse = Serenity.sessionVariableCalled(DELETE_BOOK_API_RESPONSE);
        assertThat(deleteBookApiResponse.statusCode()).isEqualTo(200);

        iViewTheBook();

        Response viewBookApiResponse = Serenity.sessionVariableCalled(VIEW_BOOK_API_RESPONSE);
        assertThat(viewBookApiResponse.statusCode()).isEqualTo(404);

    }

    @Given("I have a set of books")
    public void iHaveASetOfBooks() throws JsonProcessingException {

        List<Book> listOfBooks = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            iWantToAddNewBook();
            iAddNewBook();
            Response response = Serenity.sessionVariableCalled(CREATE_BOOK_API_RESPONSE);
            listOfBooks.add(response.as(Book.class));
        }

        Serenity.setSessionVariable(INITIAL_BOOKS).to(listOfBooks);
    }

    @When("I view all books")
    public void iViewAllBooks() {
        Serenity.setSessionVariable(VIEW_ALL_BOOKS_API_RESPONSE).to(bookshopConnector.viewAllBooks());
    }

    @Then("I can see all my books")
    public void iCanSeeAllMyBooks() {
        List<Book> allMyBooks = Serenity.sessionVariableCalled(INITIAL_BOOKS);
        Response response = Serenity.sessionVariableCalled(VIEW_ALL_BOOKS_API_RESPONSE);
        assertThat(response.statusCode()).isEqualTo(200);


        // could not remember how to convert to arraylist
        Book[] parsedResponse = response.as(Book[].class);
        List<Book> filteredResponseList =
                Arrays
                        .stream(parsedResponse)
                        .filter(book -> allMyBooks.stream().anyMatch(b -> b.getId().equals(book.getId())))
                        .collect(Collectors.toList());

        assertThat(allMyBooks).containsExactlyInAnyOrderElementsOf(filteredResponseList);

    }
}
