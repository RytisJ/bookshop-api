package com.bookshop.utils;

import com.bookshop.connectors.BookshopConnector;
import com.bookshop.models.Book;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.util.EnvironmentVariables;

import java.util.List;

import static com.bookshop.utils.SharedStateConstants.*;

/**
 * Helper to manage what actions happen before or after the tests run
 */
public class Hooks {
    private EnvironmentVariables env;

    /**
     * When tests start it saves injected value of config
     * Mainly acting for clean code
     */
    @Before
    public void setEnv() {
        Serenity.setSessionVariable(ENV).to(env);
    }

    @SneakyThrows
    private void removeBookWithRetry(Book book) {
        BookshopConnector bookshopConnector = new BookshopConnector();
        int responseStatusCode = 0;
        for (int i = 0; i < 5; i++) {
                Response response = bookshopConnector.deleteBook(book.getId());
                responseStatusCode = response.statusCode();
                if (response.statusCode() == 200) {
                    break;
                }

        }

        if (responseStatusCode != 200) {
            throw new Exception("Could not clean up test data book " + book);
        }
    }

    /**
     * Deletes test data created by tests
     */

    @After("@cleanup-book")
    public void bookCleanup() {
        Response createABookResponse = Serenity.sessionVariableCalled(CREATE_BOOK_API_RESPONSE);

        // if test failed without saving a book, no point invoking delete
        if (createABookResponse != null) {
            Book book = createABookResponse.as(Book.class);
            removeBookWithRetry(book);
        }




    }

    /**
     * Deletes test data created by tests
     */
    @SneakyThrows
    @After("@cleanup-books")
    public void booksCleanup() {
        List<Book> books = Serenity.sessionVariableCalled(INITIAL_BOOKS);

        // if test failed without saving a book, no point invoking delete
        if (books != null && books.size() != 0) {
            for (Book book : books) {
                removeBookWithRetry(book);
            }
        }

    }
}
