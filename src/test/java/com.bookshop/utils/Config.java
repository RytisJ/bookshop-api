package com.bookshop.utils;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;

import static com.bookshop.utils.SharedStateConstants.ENV;

/**
 * Helper class to manage configuration for the test project
 */
public class Config {

    /**
     * gets value based from config based on environment you are using
     * @param key of config value you want to return
     * @return value of the config key provided
     */
    public String getConfigValue(String key) {
        EnvironmentVariables env = Serenity.sessionVariableCalled(ENV);
        return EnvironmentSpecificConfiguration.from(env).getProperty(key);
    }
}
