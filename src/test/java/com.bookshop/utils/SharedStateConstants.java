package com.bookshop.utils;

/**
 * Enum to make sure that when using shared information between com.bookshop.steps is not misspelled
 */
public enum SharedStateConstants {
    INITIAL_BOOK,
    INITIAL_BOOKS,

    BOOK_ID,
    CREATE_BOOK_API_RESPONSE,
    DELETE_BOOK_API_RESPONSE,
    UPDATE_BOOK_API_RESPONSE,
    VIEW_BOOK_API_RESPONSE,
    VIEW_ALL_BOOKS_API_RESPONSE,
    ENV
}

