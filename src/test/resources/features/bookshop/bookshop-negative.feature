@cleanup-book
Feature: bookshop crud api - negative tests

  Scenario: adding a book - using bad schema
    Given I want to add new book
    When I add a book with author set as boolean
    Then I can see "Bad Request" when creating a new book

  Scenario: adding a book - bound testing
    Given I want to add new book
    When I add a book with really long name
    Then I can see "Bad Request" when creating a new book

  Scenario Outline: <typeOfAction> a book - bad id <typeOfId>
    Given I am <apiType> a book that <typeOfId>
    Then I can see "Bad Request" when <apiType>
    Examples:
      | apiType         | typeOfId                   |
      | viewing a book  | has non standard id        |
      | viewing a book  | null                       |
      | viewing a book  | empty                      |
      | viewing a book  | out of bounds - low bound  |
      | viewing a book  | out of bounds - high bound |
      | deleting a book | has non standard id        |
      | deleting a book | null                       |
      | deleting a book | empty                      |
      | deleting a book | out of bounds - low bound  |
      | deleting a book | out of bounds - high bound |
      | updating a book | has non standard id        |
      | updating a book | null                       |
      | updating a book | empty                      |
      | updating a book | out of bounds - low bound  |
      | updating a book | out of bounds - high bound |


  Scenario Outline: <apiType> a book - id that does not exist
    Given I am <apiType> a book that does not exist
    Then I can see "Not Found" when <apiType>
    Examples:
      | apiType         |
      | viewing a book  |
      | deleting a book |
      | updating a book |
