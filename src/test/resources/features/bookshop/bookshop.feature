Feature: bookshop crud api

  @smoke @cleanup-book
  Scenario: adding a book
    Given I want to add new book
    When I add new book
    Then my book is added

  @cleanup-book
  Scenario: view a book
    Given I have a book
    When I view the book
    Then I can see book information

  @cleanup-book
  Scenario: update a book
    Given I have a book
    When I update books attributes
    Then I can see the updates to the book

  Scenario: delete a book
    Given I have a book
    When I delete the book
    Then the book is deleted

  @cleanup-books
  Scenario: view all books
    Given I have a set of books
    When I view all books
    Then I can see all my books

